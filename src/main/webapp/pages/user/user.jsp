<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf8">
	   <title><spring:message code="label.titleedit" /></title>
   </head>
   <body>

      <h2><spring:message code="label.titleedit" /></h2>

        <form:form method="post" action="save" commandName="user">
            <table>
              <tr>
                 <td><form:label path="id" /></td>
                 <td><form:input type="hidden" path="id" /></td>
              </tr>
              <tr>
                 <td><form:label path="surname"><spring:message code="label.surname" /></form:label></td>
                 <td><form:input path="surname" /></td>
              </tr>
              <tr>
                 <td><form:label path="name"><spring:message code="label.name" /></form:label></td>
                 <td><form:input path="name" /></td>
              </tr>
              <tr>
                 <td><form:label path="patronymic"><spring:message code="label.patronymic" /></form:label></td>
                 <td><form:input path="patronymic" /></td>
              </tr>
              <tr>
                 <td><form:label path="position"><spring:message code="label.position" /></form:label></td>
                 <td><form:input path="position" /></td>
              </tr>
              <tr>
                 <td><form:label path="login"><spring:message code="label.login" /></form:label></td>
                 <td><form:input path="login" /></td>
              </tr>
              <tr>
                 <td><form:label path="password"><spring:message code="label.password" /></form:label></td>
                 <td><form:input path="password" /></td>
              </tr>
              <tr>
                 <td><input type="submit" value="<spring:message code="label.save" />" /></td>
                 <td>
                     <a href="users">
                        <input type="button" value="<spring:message code="label.cancel" />" />
                     </a>
                 </td>
              </tr>
            </table>
        </form:form>
   </body>
</html>