package com.flx.testingtask.api.service;

import com.flx.testingtask.impl.domain.User;

import java.util.List;

public interface UserService {

    /**
     * Saving domain to DB or update if it already exists
     *
     * @param user instance of domain
     */
    void saveOrUpdate(User user);

    /**
     * Deleting domain from database
     *
     * @param user instance that must be deleted
     */
    void delete(User user);

    /**
     * Getting domain by its id
     *
     * @param id value of field sought-for domain
     * @return domain instance where its id field equals param
     */
    User getById(Integer id);

    /**
     * Getting domain by its login
     *
     * @param login value of field sought-for domain
     * @return domain instance where its login field equals param
     */
    User getByLogin(String login);

    /**
     * Getting list of all users form database
     *
     * @return list of all users
     */
    List<User> getAll();

}
