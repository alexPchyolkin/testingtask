package com.flx.testingtask.impl.controller;

import com.flx.testingtask.api.service.UserService;
import com.flx.testingtask.impl.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/users")
    public ModelAndView getAll() {
        ModelAndView view = new ModelAndView("user/users");
        view.addObject("users", userService.getAll());
        return view;
    }

    @RequestMapping(value = "/add")
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("user/user");
        view.addObject("user", new User());
        return view;
    }

    @RequestMapping(value = "/update")
    public ModelAndView update(@RequestParam(value = "userId", required = true) Integer userId) {
        ModelAndView view = new ModelAndView("user/user");
        view.addObject("user", userService.getById(userId));
        return view;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveOrUpdate(@ModelAttribute("user") User user, BindingResult result) {
        userService.saveOrUpdate(user);
        return "redirect:/user/users";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam(value = "userId", required = true) Integer id) {
        User user = userService.getById(id);
        userService.delete(user);
        return "redirect:/user/users";
    }
}