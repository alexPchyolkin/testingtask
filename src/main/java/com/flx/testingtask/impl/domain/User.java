package com.flx.testingtask.impl.domain;

import javax.persistence.*;

@Entity
@Table(name = "application_user")
public class User {

    @SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "USERS_ID_seq", name = "USERS_ID_seq")
    @GeneratedValue(generator = "USERS_ID_seq", strategy = GenerationType.SEQUENCE)
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PATRONYMIC")
    private String patronymic;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "USER_PASS")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
